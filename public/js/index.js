function initApp() {
    var txtEmail = document.getElementById("inputEmail");
    var txtPassword = document.getElementById("inputPassword");
    var btnLogin = document.getElementById("btnLogin");
    var btnSignUp = document.getElementById("btnSignUp");
    var btnFacebook = document.getElementById("btnFacebook");
    var btnGithub = document.getElementById("btnGithub");
    var btnGoogle = document.getElementById("btnGoogle");

    // Sign up with Email/Password
    btnSignUp.addEventListener("click", function () {
        firebase
            .auth()
            .createUserWithEmailAndPassword(txtEmail.value, txtPassword.value)
            .then(() => {
                create_alert("success", "Create successfully!");
                txtEmail.value = "";
                txtPassword.value = "";
            })
            .catch((err) => {
                create_alert("error", err.message);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });
    // Log in with Email/Password
    btnLogin.addEventListener("click", function () {
        firebase
            .auth()
            .signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
            .then((result) => {
                window.location.href = "chatroom.html";
                create_alert("success", "Log in successfully.");
            })
            .catch((err) => {
                create_alert("error", err.message);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });
    // Log in with Facebook
    btnFacebook.addEventListener("click", () => {
        var provider = new firebase.auth.FacebookAuthProvider();
        firebase
            .auth()
            .signInWithPopup(provider)
            .then((result) => {
                var token = result.credential.accessToken;
                var user = result.user;
                create_alert("success", "Log in successfully.");
                window.location.href = "chatroom.html";
            })
            .catch((err) => {
                create_alert("error", err.message);
            });
    });
    // Log in with Github
    btnGithub.addEventListener("click", () => {
        var provider = new firebase.auth.GithubAuthProvider();
        firebase
            .auth()
            .signInWithPopup(provider)
            .then((result) => {
                var token = result.credential.accessToken;
                var user = result.user;
                create_alert("success", "Log in successfully.");
                window.location.href = "chatroom.html";
            })
            .catch((err) => {
                create_alert("error", err.message);
            });
    });
    // Log in with Google
    btnGoogle.addEventListener("click", () => {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase
            .auth()
            .signInWithPopup(provider)
            .then((result) => {
                var token = result.credential.accessToken;
                var user = result.user;
                create_alert("success", "Log in successfully.");
                window.location.href = "chatroom.html";
            })
            .catch((err) => {
                create_alert("error", err.message);
            });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById("custom-alert");
    if (type == "success") {
        str_html =
            "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" +
            message +
            "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html =
            "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" +
            message +
            "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};
