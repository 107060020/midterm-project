var user_email = "";
var user_name = "";
var owner = "";
var state = "Lobby";
var o_state = "Lobby";
var chatroom_list = [];
var member_list = [];
var profile_list = [];
var profile_num = 0;
var init_done = false;
var room_num = 0;
var count = 0;

function loadProfileImage() {
    if (profile_num !== member_list.length) {
        setTimeout(() => {
            loadProfileImage();
        }, 1000);
        console.log("Loading profile images...");
    } else {
        console.log("Load profile images done!");
    }
}
async function initApp() {
    Notification.requestPermission().then((p) => {
        if (p === "denied") {
            console.log("Notification denied");
        } else if (p === "granted") {
            console.log("Notification granted");
        }
    });
    await firebase.auth().onAuthStateChanged((user) => {
        if (user) {
            user_email = user.email;
            user_name = user_email.replace("@", "_").replaceAll(".", "_");
            console.log(user_name);
            $("#user-name").html("<strong>" + user_email.split("@")[0] + "</strong>");
        } else {
            //alert('Get user failed!')
        }
    });
    snapshot = await firebase.database().ref().once("value");
    room_num = await snapshot.numChildren();
    let str = "Lobby/member/" + user_name;
    if (!room_num) {
        await firebase
            .database()
            .ref(str)
            .set({ authority: "owner", email: user_email });
        window.location.reload();
    }
    let hasName = await snapshot.hasChild(str);
    if (!hasName) {
        await firebase
            .database()
            .ref(str)
            .set({ authority: "owner", email: user_email });
    }
    snapshot = await firebase.database().ref("Lobby/member").once("value");
    await snapshot.forEach((member) => {
        member_list.push(member.key);
    });
    for (const member of member_list) {
        await firebase
            .storage()
            .ref()
            .child("profile/" + member + ".png")
            .getDownloadURL()
            .then((url) => {
                profile_list[member] = url;
                profile_num++;
            })
            .catch((e) => {
                profile_list[member] = "";
                profile_num++;
            });
    }
    loadProfileImage();
    window.setTimeout(() => runApp(), 0);
}
function runApp() {
    firebase
        .database()
        .ref()
        .on("child_added", (chatroom) => {
            count++;
            let isMember = chatroom.hasChild("member/" + user_name);
            let isNewMember = false;
            chatroom = chatroom.key;
            /*
              firebase.database().ref('Lobby/member').on('child_added', member => {
                  member = member.key;
                  if (!member_list.includes(member)) member_list.push(member)
                  firebase.storage().ref().child('profile/' + member + '.png').getDownloadURL().then(url => {
                      profile_list[member] = url
                  }).catch(e => {
                      profile_list[member] = ''
                  });
              });
              */
            firebase
                .database()
                .ref(chatroom + "/member")
                .on("child_added", (data) => {
                    member = data.key;
                    if (chatroom == "Lobby" && !member_list.includes(member)) {
                        member_list.push(member);
                    }
                    let hasName = user_name === data.key;
                    if (hasName) {
                        isNewMember = true;
                        $(
                            '<li type="button" class="contact" id="' +
                            chatroom +
                            '" onclick="switchRoom(this.id)"><div class="wrap"><img id="' +
                            chatroom +
                            '-preview-image" src="" alt=""' +
                            "onerror=\"this.onerror=null;this.src='img/empty.png';\"" +
                            '/><div class="meta"><p class="name">' +
                            chatroom.split("_").pop() +
                            '</p><p class="preview"></p></div></div></li>'
                        ).appendTo($("#contacts ul"));
                        $("#" + chatroom + ".contact .preview").html(
                            "<span>no commitment</span>"
                        );
                        $(
                            '<div class="messages" id="_' +
                            chatroom +
                            '" style="display: none;"><ul></ul></div>'
                        ).appendTo($(".content"));
                    }
                    if (init_done && $("#_" + chatroom + " ul").is(":empty")) {
                        firebase
                            .database()
                            .ref(chatroom + '/message')
                            .once("value")
                            .then((snapshot) => {
                                snapshot.forEach((data) => {
                                    data = data.val();
                                    let profile =
                                        profile_list[
                                        data.email.replace("@", "_").replaceAll(".", "_")
                                        ];
                                    if (data.email != user_email) {
                                        if (data.type === "text") {
                                            $(
                                                '<li class="receive"><img src="' +
                                                profile +
                                                '" alt=""' +
                                                "onerror=\"this.onerror=null;this.src='img/icons8-profile-64.png';\"" +
                                                "/><p>" +
                                                data.message +
                                                "</p></li>"
                                            ).appendTo($("#_" + chatroom + " ul"));
                                            $("#" + chatroom + ".contact .preview").html(
                                                "<span>" +
                                                data.email.split("@")[0] +
                                                ": </span>" +
                                                data.message
                                            );
                                        } else if (data.type === "image") {
                                            $(
                                                '<li class="receive"><img src="' +
                                                profile +
                                                '" alt=""' +
                                                "onerror=\"this.onerror=null;this.src='img/icons8-profile-64.png';\"" +
                                                '/><p><img src="' +
                                                data.message +
                                                '" alt="" style="width: 100%; border-radius: 0%"></p></li>'
                                            ).appendTo($("#_" + chatroom + " ul"));
                                            $("#" + chatroom + ".contact .preview").html(
                                                "<span>" +
                                                data.email.split("@")[0] +
                                                ": </span>[image]"
                                            );
                                        }
                                    }
                                    if (profile) {
                                        document
                                            .getElementById(chatroom + "-preview-image")
                                            .setAttribute("src", profile);
                                    } else {
                                        document
                                            .getElementById(chatroom + "-preview-image")
                                            .setAttribute("src", "img/icons8-profile-64.png");
                                    }
                                });
                            });
                    }
                });
            firebase
                .database()
                .ref(chatroom + '/message')
                .on("child_added", (data) => {
                    data = data.val();
                    let profile =
                        profile_list[data.email.replace("@", "_").replaceAll(".", "_")];
                    if (data.type === "text") {
                        let str =
                            '<img src="' +
                            profile +
                            '" alt=""' +
                            "onerror=\"this.onerror=null;this.src='img/icons8-profile-64.png';\"" +
                            "/><p>" +
                            data.message +
                            "</p></li>";
                        if (data.email === user_email) {
                            $('<li class="sent">' + str).appendTo(
                                $("#_" + chatroom + " ul")
                            );
                            $("#" + chatroom + ".contact .preview").html(
                                "<span>You: </span>" + data.message
                            );
                        } else if (data.email) {
                            $('<li class="receive">' + str).appendTo(
                                $("#_" + chatroom + " ul")
                            );
                            $("#" + chatroom + ".contact .preview").html(
                                "<span>" +
                                data.email.split("@")[0] +
                                ": </span>" +
                                data.message
                            );
                            if (init_done && (isMember || isNewMember)) {
                                if (Notification.permission !== "default") {
                                    var notify = new Notification(data.email, {
                                        body: data.message,
                                        icon: profile
                                    });
                                } else {
                                    console.log("Notification denied!");
                                }
                            }
                        }
                    } else if (data.type === "image") {
                        let str =
                            '<img src="' +
                            profile +
                            '" alt=""' +
                            "onerror=\"this.onerror=null;this.src='img/icons8-profile-64.png';\"" +
                            '/><p><img src="' +
                            data.message +
                            '" alt="" style="width: 100%; border-radius: 0%"></p></li>';
                        if (data.email === user_email) {
                            $('<li class="sent">' + str).appendTo(
                                $("#_" + chatroom + " ul")
                            );
                            $("#" + chatroom + ".contact .preview").html(
                                "<span>You: </span>[image]"
                            );
                        } else if (data.email) {
                            $('<li class="receive">' + str).appendTo(
                                $("#_" + chatroom + " ul")
                            );
                            $("#" + chatroom + ".contact .preview").html(
                                "<span>" + data.email.split("@")[0] + ": </span>[image]"
                            );
                            if (init_done && (isMember || isNewMember)) {
                                if (Notification.permission !== "default") {
                                    var notify = new Notification(data.email, {
                                        image: data.message,
                                        icon: profile
                                    });
                                } else {
                                    console.log("Notification denied!");
                                }
                            }
                        }
                    }
                    if (profile) {
                        document
                            .getElementById(chatroom + "-preview-image")
                            .setAttribute("src", profile);
                    } else {
                        document
                            .getElementById(chatroom + "-preview-image")
                            .setAttribute("src", "img/icons8-profile-64.png");
                    }
                    $("#_" + chatroom + ".messages").animate(
                        { scrollTop: $("#_" + chatroom + ".messages")[0].scrollHeight },
                        "fast"
                    );
                });
            if (count === room_num) {
                init_done = true;
                let profile = profile_list[user_name];
                if (profile) {
                    document.getElementById("profile-image").setAttribute("src", profile);
                } else {
                    document
                        .getElementById("profile-image")
                        .setAttribute("src", "img/icons8-profile-64.png");
                }
                switchRoom("Lobby");
            }
        });
    $(".messages").animate(
        {
            scrollTop: $(document).height()
        },
        "fast"
    );
    $("#profile-image").click(() => {
        $("#option").toggleClass("active");
    });
    $("#profile-change").click(() => {
        $("#option").removeClass("active");
        document.getElementById("change-profile").click();
    });
    $("#logout").click(() => {
        firebase
            .auth()
            .signOut()
            .then(() => {
                alert("Log out successfully!");
                document.location.href = "index.html";
            })
            .catch((err) => {
                alert(err.message);
            });
        $("#option").removeClass("active");
    });
    $(".create-room").click(() => {
        let chatroom = newRoom();
        if (chatroom_list.includes(chatroom)) {
            alert("Duplicate room name!");
            return;
        }
        chatroom_list.push(chatroom);
        firebase
            .database()
            .ref(chatroom + "/member/" + user_name)
            .set({ authority: "owner", email: user_email });
        switchRoom(chatroom);
    });
    $(".add-member").click(() => {
        let member_mail = newMember();
        let member = member_mail.replace("@", "_").replaceAll(".", "_")
        if (!member) {
            alert("You may need to enter something!");
            return;
        } else if (state === "Lobby") {
            alert("You cannot add anyone into lobby!");
            return;
        } else if (user_email != owner) {
            alert("Permission denied! You are not the owner!");
            return;
        } else if (!member_list.includes(member)) {
            alert("There is no such user in this application!");
            return;
        }
        firebase
            .database()
            .ref(state + "/member")
            .once("value")
            .then((chatroom) => {
                let hasName = chatroom.hasChild(member);
                if (hasName) {
                    alert("Already in room!");
                    return;
                }
                firebase
                    .database()
                    .ref(state + "/member/" + member.replace("@", "_").replaceAll(".", "_"))
                    .set({ authority: "member", email: member_mail });
            })
            .catch((e) => console.log(e.message));
    });
    $("#submit-img").click(() => {
        document.getElementById("new-image").click();
    });
    $(".submit").click(() => {
        newMessage();
    });
    $(window).on("keydown", (e) => {
        if (e.which == 13) {
            newMessage();
            return false;
        }
    });
}
function newMessage() {
    message = $(".message-input input").val();
    if ($.trim(message) == "") {
        return false;
    } else if (isHTML(message)) {
        $(".message-input input").val(null);
        alert('Your message include html code!')
        return false;
    }
    firebase.database().ref(state + '/message').push({
        type: "text",
        email: user_email,
        message: message
    });
    $(".message-input input").val(null);
}
async function newImage(e) {
    let file = e.target.files[0];
    uuid = uuidv4();
    await firebase
        .storage()
        .ref()
        .child(state + "/" + uuid + ".png")
        .put(file);
    firebase
        .storage()
        .ref()
        .child(state + "/" + uuid + ".png")
        .getDownloadURL()
        .then((url) => {
            firebase.database().ref(state + '/message').push({
                type: "image",
                email: user_email,
                message: url
            });
        })
        .catch((e) => console.log(e.message));
}
function newRoom() {
    let roomName = user_name + "_" + $("#room-name").val();
    $("#room-name").val(null);
    return roomName;
}
function newMember() {
    let memberName = $("#member-name").val();
    $("#member-name").val(null);
    return memberName;
}
async function changeProfile(e) {
    let file = e.target.files[0];
    await firebase
        .storage()
        .ref()
        .child("profile/" + user_name + ".png")
        .put(file);
    window.location.reload();
}
async function switchRoom(id) {
    o_state = state;
    document.getElementById("_" + o_state).style.display = "none";
    document.getElementById("_" + id).style.display = "block";
    $("#" + o_state).removeClass("active");
    $("#" + id).addClass("active");
    state = id;
    await firebase
        .database()
        .ref(state + "/member")
        .once("value")
        .then((snapshot) => {
            snapshot.forEach((member) => {
                member = member.val();
                if (member.authority == "owner") {
                    owner = member.email;
                }
            });
        });
    $(".chatroom-title").html("<strong>" + state.split("_").pop() + "</strong>");
}
function isHTML(str) {
    return str !== (str || '').replace(/<([^>]+?)([^>]*?)>(.*?)<\/\1>/ig, '').replace(/(<([^>]+)>)/ig, '').trim();
}
function uuidv4() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
        var r = (Math.random() * 16) | 0,
            v = c == "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
}
window.onload = () => {
    initApp();
};
