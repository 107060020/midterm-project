# Software Studio 2021 Spring Midterm Project

## Website Link
>https://chatapp-6666.firebaseapp.com 

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

* Membership Mechanism / Firebase Page / Database / RWD
![](https://i.imgur.com/uzQxItn.gif)

* Topic Key Function
    * "Lobby" is available to every user.
    ![](https://i.imgur.com/xLo53AF.gif)

    * Create a new room and add an member.
    ![](https://i.imgur.com/qeKIGTl.gif)
    
    * Members who are not the owner do not have permission to add new members.
    ![](https://i.imgur.com/l9aHUkL.gif)![](https://i.imgur.com/dhQyY1N.gif)

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

* Third-Party Sign In
![](https://i.imgur.com/47wtipO.gif)
* Chrome Notification / Use CSS Animation
    > Already shown in previous gifs.
* Security Report
![](https://i.imgur.com/P6F7hzy.gif)

## Additional Components
* Change profile image & message preview
![](https://i.imgur.com/IHPwOy2.gif)
